## Build the docker image and deploy locally 
1. Build the docker container image  
`docker build -t isce2 .`  
2. Run a container   
* JupyterLab interface: https://gitlab.com/sbakergeo/docker-isce2/snippets/1996022
* Classic notebook interface: https://gitlab.com/sbakergeo/docker-isce2/snippets/1996030

### run a container from the [registry](https://gitlab.com/sbakergeo/docker-isce2/container_registry):  

`docker run -d --rm -p 8888:8888 -v $HOME:/home/jovyan/work -e JUPYTER_LAB_ENABLE=yes --name isce2 registry.gitlab.com/sbakergeo/docker-isce2 start.sh jupyter lab --LabApp.token=''`


## Running with Singularity
https://sylabs.io/singularity-desktop-macos/ 

`singularity pull isce2.sif docker://registry.gitlab.com/sbakergeo/docker-isce2`    
`singularity exec isce2.sif stripmapApp.py -h`  
