FROM jupyter/scipy-notebook
MAINTAINER baker "baker@unavco.org"

USER $NB_UID
RUN conda install --yes --quiet \ 
    ipywidgets \
    dask \ 
    numba \
    sympy \
    netcdf4 \
    lxml \  
    scikit-learn \ 
    scikit-image \ 
    pandas \ 
    pyhdf \ 
    requests \ 
    pyresample \ 
    cartopy \ 
    cdsapi \
    cvxopt \ 
    dask-jobqueue>=0.3 \
    ecCodes \
    pygrib \
    pykdtree \
    pyproj \
    rise \ 
    xlrd \ 
    cython \ 
    gdal>=3.0.0 \ 
    h5py \
    pytest \
    numpy \
    fftw \
    scipy \
    basemap \
    scons \
    cmake \
    armadillo \
    opencv && \
    conda clean --all -f -y && \
    # Activate ipywidgets extension in the environment that runs the notebook server
    jupyter nbextension enable --py widgetsnbextension --sys-prefix && \
    jupyter nbextension enable rise --py --sys-prefix && \
    jupyter lab build --dev-build=False && \
    npm cache clean --force && \
    rm -rf $CONDA_DIR/share/jupyter/lab/staging && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    rm -rf /home/$NB_USER/.node-gyp && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

#############################################
### Jupyter Extensions
#############################################
RUN python3 -m pip install jupyter_nbextensions_configurator jupyter_contrib_nbextensions jupyter_dashboards ipympl 
RUN conda install hide_code
RUN jupyter contrib nbextension install --user
RUN jupyter nbextensions_configurator enable --user
RUN jupyter nbextension enable --py hide_code
RUN jupyter serverextension enable --py hide_code
# Install the jupyter dashboards.
RUN jupyter dashboards quick-setup --sys-prefix
RUN jupyter nbextension enable jupyter_dashboards --py --sys-prefix
# Install interactive matplotlib
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
RUN jupyter labextension install jupyter-matplotlib jupyter-threejs jupyterlab-datawidgets

#############################################
### Libraries and Dependencies
#############################################
## For MintPy
RUN GEOS_DIR=/opt/conda/lib/ CPATH=/opt/conda/include/ python3 -m pip install git+https://github.com/matplotlib/basemap.git
RUN python3 -m pip install git+https://github.com/tylere/pykml.git

USER root
RUN apt-get update && \
    apt-get install -y ssh wget rsync bzip2 csh vim git autoconf gcc g++ gfortran libmotif-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
#############################################
### InSAR Processing and Analysis Software
#############################################
### ISCE ###
RUN ln -s /opt/conda/bin/cython /opt/conda/bin/cython3 # need for compiling, see isce2/README.md
RUN git clone https://github.com/isce-framework/isce2.git /usr/local/isce2
COPY SConfigISCE /usr/local/isce2
RUN cd /usr/local/isce2 && \
    export PYTHONPATH=/usr/local/isce2/configuration && \
    export SCONS_CONFIG_DIR=/usr/local/isce2 && \
    scons install
### FRInGE ###
#RUN mkdir /usr/local/FRInGE
#RUN cd /usr/local/FRInGE && mkdir install build
#RUN git clone https://github.com/isce-framework/fringe.git /usr/local/FRInGE/src/fringe && \
#    cd /usr/local/FRInGE/src && \
#    CXX=g++ cmake -DCMAKE_INSTALL_PREFIX=../install ../src/fringe && \
#    make all && make install
### MintPy ###
RUN git clone https://github.com/insarlab/MintPy.git /usr/local/MintPy
RUN git clone https://github.com/AngeliqueBenoit/pyaps3.git /usr/local/pyaps3
RUN rm -rf /tmp/*

USER $NB_UID
ENV ISCE_ROOT /usr/local
ENV ISCE_HOME $ISCE_ROOT/isce
ENV PROJ_LIB /opt/conda/share/proj
ENV CPL_ZIP_ENCODING UTF-8
ENV MINTPY_HOME /usr/local/MintPy
ENV PYTHONPATH $ISCE_ROOT:$ISCE_HOME/applications:$ISCE_HOME/components:$MINTPY_HOME:/usr/local/pyaps3:/usr/local/FRInGE/install/python
ENV PATH $ISCE_HOME/bin:$ISCE_HOME/applications:$MINTPY_HOME/mintpy:$MINTPY_HOME/sh:/usr/local/FRInGE/install/bin:$PATH
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/FRInGE/install/lib
ENV OMP_NUM_THREADS 2
